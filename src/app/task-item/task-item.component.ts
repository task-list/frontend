import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tarefa } from 'src/app/tarefa';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {
  @Input() tarefa: Tarefa;
  edit:boolean = false;
  @Output() deleteTarefa = new EventEmitter();
  @Output() salvaTarefa = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  editar(){
    this.edit= !this.edit;
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.salvar();
    }
  }

  enviarDadosPai(){
    this.salvaTarefa.emit(this.tarefa);
  }

  excluir(){
    this.deleteTarefa.emit(this.tarefa);
  }
  salvar(){
    this.editar();
    this.enviarDadosPai();
  }

  changecheck(){
    this.enviarDadosPai();
  }


}
