import { Component, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';
import { Tarefa } from 'src/app/tarefa';
import { Http } from '@angular/http';
import { environment } from '../environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'app';
  LISTA_ITENS = 'LISTA_ITENS';
  subs = new Subscription();
  tarefas: Tarefa[] = new Array;
  inserir: string;
  constructor(private dragulaService: DragulaService, private _http: Http) {

    this.subs.add(dragulaService.dropModel("LISTA_ITENS")
      .subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
        console.log(targetModel);
        let self = this;
        this._http.put(environment.server + "tarefas",targetModel).toPromise().then
        (function (data) {
          let body = data.json() || [];
          self.parse(body);
        })

      }))


  }

  parse(body) {
    let lista = [];
    body.forEach(item => {
      let tarefa: Tarefa = new Tarefa;
      tarefa.finalizada = item.finalizada;
      tarefa.texto = item.texto;
      tarefa.id = item.id;
      tarefa.ordenador = item.ordenador;
      lista.push(tarefa);
    });
    this.tarefas = lista;
  }

  deleteTarefa(tarefa){
    let self = this;
    this._http.delete(environment.server + "tarefas/"+tarefa.id).toPromise().then
    (function (data) {
      let body = data.json() || [];
      self.parse(body);
    })
  }

  salvaTarefa(){
    let self = this;
    this._http.put(environment.server + "tarefas",this.tarefas).toPromise().then
    (function (data) {
      let body = data.json() || [];
      self.parse(body);
    })
  }

  Adicionar() {
    let tarefa: Tarefa = new Tarefa;
    tarefa.finalizada = false;
    tarefa.texto = this.inserir;
    let self = this;
    this._http.post(environment.server + "tarefas",tarefa).toPromise().then
    (function (data) {
      let body = data.json() || [];
      self.parse(body);
    })
  }
  
  ngOnInit(): void {
    let self = this;
    this._http.get(environment.server + "tarefas").toPromise().then
      (function (data) {
        let body = data.json() || [];
        self.parse(body);
      });
  }
}
